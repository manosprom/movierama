create table user_movie_reactions
(
    id bigserial primary key not null,
    uuid uuid not null,
    movie_id bigint not null ,
    user_id bigint not null,
    created_at timestamp not null,
    is_like bool not null
);

alter table user_movie_reactions
    add constraint fk_user_movie_reactions_to_movie foreign key (movie_id) references movies(id),
    add constraint fk_user_movie_reactions_to_user foreign key (user_id) references users(id);

create unique index uidx_user_movie_reaction on user_movie_reactions (movie_id, user_id);