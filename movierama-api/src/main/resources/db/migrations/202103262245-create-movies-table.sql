create table movies
(
    id          bigserial primary key not null,
    uuid        uuid                  not null,
    title       varchar(50)           not null,
    description varchar(500)          not null,
    user_id     bigint                not null,
    created_at  timestamp             not null
);

alter table movies
    add constraint fk_movies_to_users foreign key (user_id) references users (id);

create unique index uidx_movies_uuid on movies (uuid);
create unique index uidx_movies_title on movies (UPPER(title));