create table users
(
    id               bigserial primary key not null,
    uuid             uuid                  not null,
    username         varchar(50)           not null,
    password_encoded varchar(256)          not null,
    created_at       timestamp             not null
);

create unique index uidx_users_uuid on users (uuid);
create unique index uidx_users_username on users (UPPER(username));