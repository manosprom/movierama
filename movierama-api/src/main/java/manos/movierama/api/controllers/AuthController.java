package manos.movierama.api.controllers;

import static manos.movierama.api.exceptions.ApiException.DuplicateRecordException.usernameTaken;

import javax.validation.Valid;
import manos.movierama.api.controllers.views.auth.LoginRequest;
import manos.movierama.api.controllers.views.auth.LoginResponse;
import manos.movierama.api.controllers.views.auth.SignupRequest;
import manos.movierama.api.security.JwtUtils;
import manos.movierama.api.security.UserPrincipal;
import manos.movierama.api.services.AuthenticationService;
import manos.movierama.api.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController extends ApiController {
    private final JwtUtils jwtUtils;
    private final AuthenticationService authenticationService;

    private final UserService userService;

    public AuthController(JwtUtils jwtUtils, AuthenticationService authenticationService, UserService userService) {
        this.jwtUtils = jwtUtils;
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationService.authenticate(loginRequest.getUsername(), loginRequest.getPassword());
        UserPrincipal userPrincipal = authenticationService.getPrincipal();
        String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new LoginResponse(jwt, userPrincipal.getUsername()));
    }

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            throw usernameTaken(signUpRequest.getUsername());
        }

        userService.createUser(signUpRequest);
        Authentication authentication = authenticationService.authenticate(signUpRequest.getUsername(), signUpRequest.getPassword());
        UserPrincipal userPrincipal = authenticationService.getPrincipal();
        String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new LoginResponse(jwt, userPrincipal.getUsername()));
    }
}