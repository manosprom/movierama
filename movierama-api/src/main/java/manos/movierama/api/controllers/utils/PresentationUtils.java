package manos.movierama.api.controllers.utils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class PresentationUtils {
    public static String createPostedBeforeText(LocalDateTime movieCreatedAt){
        LocalDateTime now = LocalDateTime.now();

        long years = movieCreatedAt.until( now, ChronoUnit.YEARS );
        movieCreatedAt = movieCreatedAt.plusYears( years );

        if(years >0 ){
            return years + " years ago";
        }

        long months = movieCreatedAt.until( now, ChronoUnit.MONTHS );
        movieCreatedAt = movieCreatedAt.plusMonths( months );

        if(months > 0){
            return months + " months ago";
        }

        long days = movieCreatedAt.until( now, ChronoUnit.DAYS );
        movieCreatedAt = movieCreatedAt.plusDays( days );

        if(days > 0){
            return days + " days ago";
        }

        long hours = movieCreatedAt.until( now, ChronoUnit.HOURS );
        movieCreatedAt = movieCreatedAt.plusHours( hours );

        if(hours > 0){
            return hours + " hours ago";
        }

        long minutes = movieCreatedAt.until( now, ChronoUnit.MINUTES );
        movieCreatedAt = movieCreatedAt.plusMinutes( minutes );

        if(minutes > 0){
            return minutes + " minutes ago";
        }

        long seconds = movieCreatedAt.until( now, ChronoUnit.SECONDS );

        if(seconds > 0){
            return seconds + " seconds ago";
        }

        return "just now";
    }
}
