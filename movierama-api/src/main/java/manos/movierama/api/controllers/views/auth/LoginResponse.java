package manos.movierama.api.controllers.views.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LoginResponse {

	@JsonProperty("token")
	private String token;

	@JsonProperty("type")
	private String type = "Bearer";

	@JsonProperty("username")
	private String username;

	public LoginResponse(String token, String username) {
		this.token = token;
		this.username = username;
	}
}