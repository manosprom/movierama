package manos.movierama.api.controllers.views.movies;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;

@Getter
public class MovieCreateRequest {

    @NotBlank(message = "Title should not be empty")
    @Size(min = 1, max = 50, message = "Title should between 1 and 50 chars")
    @JsonProperty("title")
    private String title;

    @Size(min = 0, max = 500)
    @JsonProperty("description")
    private String description;
}
