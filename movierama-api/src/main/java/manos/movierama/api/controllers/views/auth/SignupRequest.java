package manos.movierama.api.controllers.views.auth;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;

@Getter
public class SignupRequest {
    @NotBlank(message = "Username should not be empty")
    @Size(min = 5, max = 20)
    private String username;

    @NotBlank(message = "Password should not be empty")
    @Size(min = 8, max = 10, message = "Password should have size between 8 and 10")
    private String password;
}