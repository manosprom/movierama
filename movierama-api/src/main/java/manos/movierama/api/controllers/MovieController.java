package manos.movierama.api.controllers;

import static java.util.stream.Collectors.toList;

import java.util.List;
import javax.validation.Valid;
import manos.movierama.api.controllers.views.movies.MovieCreateRequest;
import manos.movierama.api.controllers.views.movies.MovieResponse;
import manos.movierama.api.data.queries.MovieDetails;
import manos.movierama.api.services.MoviesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movies")
public class MovieController extends ApiController {

    private final MoviesService moviesService;

    public MovieController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @PostMapping
    public ResponseEntity<MovieResponse> createMovie(@RequestBody @Valid MovieCreateRequest movieCreateRequest) {
        MovieDetails movie = moviesService.createMovie(movieCreateRequest);
        return ResponseEntity.ok(MovieResponse.from(movie));
    }

    @GetMapping
    public ResponseEntity<List<MovieResponse>> getMovies(
            @RequestParam(value = "author", required = false) String author
    ) {
        List<MovieDetails> movies;
        if (author != null) {
            movies = moviesService.getAllMovieDetails(author);
        } else {
            movies = moviesService.getAllMovieDetails();
        }

        return ResponseEntity.ok(movies.stream().map(MovieResponse::from).collect(toList()));
    }

    @PutMapping("/{movieName}/like")
    public ResponseEntity<MovieResponse> likeMovie(
            @PathVariable("movieName") String movieName
    ) {
        MovieDetails movieDetails = moviesService.like(movieName);
        return ResponseEntity.ok(MovieResponse.from(movieDetails));
    }

    @PutMapping("/{movieName}/hate")
    public ResponseEntity<MovieResponse> hateMovie(
            @PathVariable("movieName") String movieName
    ) {
        MovieDetails movieDetails = moviesService.hate(movieName);
        return ResponseEntity.ok(MovieResponse.from(movieDetails));
    }
}
