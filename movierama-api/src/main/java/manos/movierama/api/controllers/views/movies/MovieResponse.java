package manos.movierama.api.controllers.views.movies;

import static manos.movierama.api.controllers.utils.PresentationUtils.createPostedBeforeText;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import manos.movierama.api.data.queries.MovieDetails;

@Data
@Builder
@AllArgsConstructor
public class MovieResponse {

    @JsonProperty("uuid")
    private UUID uuid;

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonProperty("author")
    private String author;

    @JsonProperty("likes")
    private Long likes;

    @JsonProperty("hates")
    private Long hates;

    @JsonProperty("userReaction")
    private Boolean userReaction;

    @JsonProperty("postedBefore")
    private String postedBefore;

    public static MovieResponse from(MovieDetails movieDetails){
        return MovieResponse.builder()
                .uuid(movieDetails.getUuid())
                .title(movieDetails.getTitle())
                .description(movieDetails.getDescription())
                .author(movieDetails.getAuthor())
                .likes(movieDetails.getLikes())
                .hates(movieDetails.getHates())
                .userReaction(movieDetails.getUserReaction())
                .postedBefore(createPostedBeforeText(movieDetails.getCreatedAt()))
                .build();
    }
}
