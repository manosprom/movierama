package manos.movierama.api.services;

import java.util.Optional;
import manos.movierama.api.controllers.views.auth.SignupRequest;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.data.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserService(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public User createUser(SignupRequest signupRequest){
        User user = User.signUp(signupRequest.getUsername(), encoder.encode(signupRequest.getPassword()));
        return userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsernameIgnoringCase(username);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsernameIgnoringCase(username);
    }
}
