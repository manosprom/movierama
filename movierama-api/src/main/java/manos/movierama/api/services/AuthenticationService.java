package manos.movierama.api.services;

import lombok.extern.slf4j.Slf4j;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.exceptions.ApiException.AuthenticationException;
import manos.movierama.api.security.UserPrincipal;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthenticationService {
    private final AuthenticationManager authenticationManager;

    public AuthenticationService(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public Authentication getAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public boolean isAuthenticated(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

    public UserPrincipal getPrincipal(){
        return (UserPrincipal) getAuthentication().getPrincipal();
    }

    public Authentication authenticate(String username, String password) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }

    public User requireAuthentication(){
        if(!this.isAuthenticated()){
            throw AuthenticationException.reactionRequiresAuthentication();
        }

        return this.getPrincipal().getUser();
    }
}
