package manos.movierama.api.services;

import static manos.movierama.api.data.entities.UserMovieReaction.createHateReaction;
import static manos.movierama.api.data.entities.UserMovieReaction.createLikeReaction;
import static manos.movierama.api.exceptions.ApiException.BadDataException.cannotLikeOwnMovies;
import static manos.movierama.api.exceptions.ApiException.DuplicateRecordException.movieAlreadySubmitted;
import static manos.movierama.api.exceptions.ApiException.RecordNotFoundException.movieNotFound;
import static manos.movierama.api.exceptions.ApiException.RecordNotFoundException.userNotFound;
import static org.springframework.data.mapping.Alias.ofNullable;

import java.util.List;
import java.util.Optional;
import manos.movierama.api.controllers.views.movies.MovieCreateRequest;
import manos.movierama.api.data.entities.Movie;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.data.entities.UserMovieReaction;
import manos.movierama.api.data.queries.MovieDetails;
import manos.movierama.api.data.repositories.MovieRepository;
import manos.movierama.api.data.repositories.UserMovieReactionRepository;
import manos.movierama.api.data.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class MoviesService {

    private final AuthenticationService authenticationService;
    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final UserMovieReactionRepository userMovieReactionRepository;

    public MoviesService(
            AuthenticationService authenticationService,
            UserRepository userRepository, MovieRepository movieRepository,
            UserMovieReactionRepository userMovieReactionRepository
    ) {
        this.authenticationService = authenticationService;
        this.movieRepository = movieRepository;
        this.userMovieReactionRepository = userMovieReactionRepository;
        this.userRepository = userRepository;
    }

    public MovieDetails createMovie(MovieCreateRequest movieCreateRequest) {
        User authenticatedUser = authenticationService.requireAuthentication();
        Optional<Movie> byTitle = movieRepository.findByTitleIgnoringCase(movieCreateRequest.getTitle());
        if (byTitle.isPresent()) {
            throw movieAlreadySubmitted(movieCreateRequest.getTitle());
        }

        Movie movie = new Movie();
        movie.setTitle(movieCreateRequest.getTitle());
        movie.setDescription(movieCreateRequest.getDescription());
        movie.setUser(authenticatedUser);
        Movie savedMovie = movieRepository.save(movie);
        return movieRepository.findMovieDetails(savedMovie.getId(), authenticatedUser.getId()).get();
    }

    public List<MovieDetails> getAllMovieDetails() {
        return getAllMovieDetails(null);
    }

    public List<MovieDetails> getAllMovieDetails(String authorFilter) {
        if (authenticationService.isAuthenticated()) {
            Long authenticatedUserId = authenticationService.getPrincipal().getUser().getId();

            Long authorId = -1L;
            if(ofNullable(authorFilter).isPresent()){
                authorId = userRepository.findByUsernameIgnoringCase(authorFilter).orElseThrow(() -> userNotFound(authorFilter)).getId();
            }
            return movieRepository.findAllMovieDetails(authenticatedUserId, authorId);
        } else {
            return movieRepository.findAllMovieDetails();
        }
    }

    public MovieDetails like(String movieName) {
        User authenticatedUser = authenticationService.requireAuthentication();
        Movie movie = movieRepository.findByTitleIgnoringCase(movieName).orElseThrow(() -> movieNotFound(movieName));

        if (movie.isOwnedBy(authenticatedUser)) {
            throw cannotLikeOwnMovies(authenticatedUser.getUsername(), movie.getTitle());
        }

        Optional<UserMovieReaction> foundReaction = userMovieReactionRepository.getUserMovieReactionByUserIdAndMovieId(authenticatedUser.getId(), movie.getId());

        if (foundReaction.isPresent()) {
            UserMovieReaction userMovieReaction = foundReaction.get();
            if (userMovieReaction.isHate()) {
                userMovieReaction.makeLike();
                userMovieReactionRepository.save(userMovieReaction);
            } else {
                userMovieReactionRepository.delete(foundReaction.get());
            }
        } else {
            UserMovieReaction userMovieReaction = createLikeReaction(movie, authenticatedUser);
            userMovieReactionRepository.save(userMovieReaction);
        }

        return movieRepository.findMovieDetails(movie.getId(), authenticatedUser.getId()).get();
    }

    public MovieDetails hate(String movieName) {
        User authenticatedUser = authenticationService.requireAuthentication();
        Movie movie = movieRepository.findByTitleIgnoringCase(movieName).orElseThrow(() -> movieNotFound(movieName));

        if (movie.isOwnedBy(authenticatedUser)) {
            throw cannotLikeOwnMovies(authenticatedUser.getUsername(), movie.getTitle());
        }

        Optional<UserMovieReaction> foundReaction = userMovieReactionRepository.getUserMovieReactionByUserIdAndMovieId(authenticatedUser.getId(), movie.getId());
        if (foundReaction.isPresent()) {
            UserMovieReaction userMovieReaction = foundReaction.get();
            if (userMovieReaction.isLike()) {
                userMovieReaction.makeHate();
                userMovieReactionRepository.save(userMovieReaction);
            } else {
                userMovieReactionRepository.delete(foundReaction.get());
            }
        } else {
            UserMovieReaction userMovieReaction = createHateReaction(movie, authenticatedUser);
            userMovieReactionRepository.save(userMovieReaction);
        }

        return movieRepository.findMovieDetails(movie.getId(), authenticatedUser.getId()).get();

    }
}
