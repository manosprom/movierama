package manos.movierama.api.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
public class ApiErrorResponse {

    @JsonProperty("code")
    private String code;

    @JsonProperty("details")
    private List<String> details = new ArrayList<>();

    public ApiErrorResponse(String code, String message) {
        this.code = code;
        details.add(message);
    }

    public ApiErrorResponse(String code, List<String> details) {
        this.code = code;
        this.details = details;
    }
}
