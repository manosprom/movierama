package manos.movierama.api.exceptions;

import static java.lang.String.format;

import lombok.Data;

@Data
public class ApiException extends RuntimeException {

    protected final String errorCode;

    protected final String errorMessage;

    public ApiException(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public static class AuthenticationException extends ApiException {

        public AuthenticationException(String errorCode, String errorMessage) {
            super(errorCode, errorMessage);
        }

        public static AuthenticationException authTokenExpired() {
            return new AuthenticationException("AUTHENTICATION_TOKEN_EXPIRED", "Your authentication token has expired");
        }

        public static AuthenticationException reactionRequiresAuthentication() {
            return new AuthenticationException("AUTHENTICATE_TO_REACT", "reaction required authentication");
        }
    }

    public static class BadDataException extends ApiException {

        public BadDataException(String errorCode, String errorMessage) {
            super(errorCode, errorMessage);
        }

        public static BadDataException cannotLikeOwnMovies(String username, String title) {
            return new BadDataException("CANNOT_LIKE_OWN_MOVIES", format("user %s owns movie %s", username, title));
        }
    }


    public static class RecordNotFoundException extends ApiException {

        public RecordNotFoundException(String errorCode, String errorMessage) {
            super(errorCode, errorMessage);
        }

        public static RecordNotFoundException userNotFound(String username) {
            return new RecordNotFoundException("USER_NOT_FOUND", String.format("User does not exist (%s)", username));
        }

        public static RecordNotFoundException movieNotFound(String movieName) {
            return new RecordNotFoundException("MOVIE_NOT_FOUND", String.format("Movie does not exist (%s)", movieName));
        }
    }

    public static class DuplicateRecordException extends ApiException {

        public DuplicateRecordException(String errorCode, String errorMessage) {
            super(errorCode, errorMessage);
        }

        public static DuplicateRecordException usernameTaken(String username) {
            return new DuplicateRecordException("USERNAME_TAKEN", String.format("Username taken (%s)", username));
        }

        public static RecordNotFoundException movieAlreadySubmitted(String movieName) {
            return new RecordNotFoundException("MOVIE_ALREADY_EXISTS", String.format("Movie (%s) already exists", movieName));
        }
    }
}
