package manos.movierama.api.exceptions;

import java.util.ArrayList;
import java.util.List;
import manos.movierama.api.exceptions.ApiException.AuthenticationException;
import manos.movierama.api.exceptions.ApiException.BadDataException;
import manos.movierama.api.exceptions.ApiException.DuplicateRecordException;
import manos.movierama.api.exceptions.ApiException.RecordNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ApiErrorResponse> handleAllException(ApiException ex) {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(new ApiErrorResponse(ex.getErrorCode(), ex.getErrorMessage()));
    }

    @ExceptionHandler(BadDataException.class)
    public ResponseEntity<ApiErrorResponse> handleRecordNotFoundException(BadDataException badDataException){
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ApiErrorResponse(badDataException.getErrorCode(), badDataException.getErrorMessage()));
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ApiErrorResponse> handleRecordNotFoundException(AuthenticationException authenticationException){
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ApiErrorResponse(authenticationException.getErrorCode(), authenticationException.getErrorMessage()));
    }

    @ExceptionHandler(RecordNotFoundException.class)
    public ResponseEntity<ApiErrorResponse> handleRecordNotFoundException(RecordNotFoundException recordNotFoundException){
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ApiErrorResponse(recordNotFoundException.getErrorCode(), recordNotFoundException.getErrorMessage()));
    }

    @ExceptionHandler(DuplicateRecordException.class)
    public ResponseEntity<ApiErrorResponse> handleDuplicateRecordException(DuplicateRecordException duplicateRecordException){
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(new ApiErrorResponse(duplicateRecordException.getErrorCode(), duplicateRecordException.getErrorMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ApiErrorResponse validationErrorApiResponse = new ApiErrorResponse("VALIDATION_ERROR", details);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationErrorApiResponse);
    }
}