package manos.movierama.api.data.entities;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "user_movie_reactions")
public class UserMovieReaction {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Getter
    @Column(name = "uuid")
    private UUID uuid;

    @Getter
    @NotNull
    @Column (name = "is_like", nullable = true)
    private Boolean isLike;

    @Getter
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id", nullable = false)
    private Movie movie;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @PrePersist
    private void prePersist() {
        uuid = UUID.randomUUID();
        createdAt = LocalDateTime.now();
    }

    public static UserMovieReaction createLikeReaction(Movie movie, User user){
        return UserMovieReaction.builder()
                .movie(movie)
                .user(user)
                .isLike(true)
                .build();
    }

    public static UserMovieReaction createHateReaction(Movie movie, User user){
        return UserMovieReaction.builder()
                .movie(movie)
                .user(user)
                .isLike(false)
                .build();
    }

    public boolean isLike() {
        return isLike;
    }

    public boolean isHate(){
        return !isLike;
    }

    public void makeHate(){
        this.isLike = false;
    }

    public void makeLike(){
        this.isLike = true;
    }
}
