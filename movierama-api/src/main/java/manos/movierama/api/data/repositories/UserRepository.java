package manos.movierama.api.data.repositories;

import java.util.Optional;
import java.util.UUID;
import manos.movierama.api.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameIgnoringCase(String username);

    boolean existsByUsernameIgnoringCase(String username);

    Optional<User> findByUuid(UUID uuid);
}
