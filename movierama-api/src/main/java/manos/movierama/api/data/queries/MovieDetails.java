package manos.movierama.api.data.queries;

import java.time.LocalDateTime;
import java.util.UUID;

public interface MovieDetails {

    UUID getUuid();

    String getTitle();

    String getDescription();

    String getAuthor();

    LocalDateTime getCreatedAt();

    Long getLikes();

    Long getHates();

    Boolean getUserReaction();
}
