package manos.movierama.api.data.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "uuid")
    private UUID uuid;

    @NotBlank
    @Size(max = 50)
    @Column(name = "username", nullable = false)
    private String username;

    @NotBlank
    @Size(max = 256)
    @Column(name = "password_encoded", nullable = false)
    private String passwordEncoded;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Movie> movies = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "movie")
    private List<UserMovieReaction> reactions = new ArrayList<>();

    public static User signUp(String username, String passwordEncoded) {
        return new User(null, null, username, passwordEncoded, null);
    }

    @Builder
    private User(Long id, @NotBlank UUID uuid, @NotBlank @Size(max = 50) String username, @NotBlank @Size(max = 256) String passwordEncoded, LocalDateTime createdAt) {
        this.id = id;
        this.uuid = uuid;
        this.username = username;
        this.passwordEncoded = passwordEncoded;
    }

    @PrePersist
    private void prePersist() {
        uuid = UUID.randomUUID();
        createdAt = LocalDateTime.now();
    }
}
