package manos.movierama.api.data.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import manos.movierama.api.data.entities.Movie;
import manos.movierama.api.data.queries.MovieDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Optional<Movie> findByUuid(UUID uuid);

    Optional<Movie> findByTitleIgnoringCase(String title);

    @Query(nativeQuery = true, value =
            "select " +
                "cast(m.uuid as varchar) as uuid, " +
                "m.title as title, " +
                "m.description as description, " +
                "m.user_id as userId, " +
                "m.created_at as createdAt, " +
                "author.username as author, " +
                "count(case when reactions.is_like then 1 end) as likes, " +
                "count(case when not reactions.is_like then 1 end) as hates, " +
                "user_reaction.is_like as userReaction " +
            "from movies m " +
            "left join user_movie_reactions reactions ON m.id = reactions.movie_id " +
            "left join user_movie_reactions user_reaction ON m.id = user_reaction.movie_id and user_reaction.user_id = :userId " +
            "left join users author on m.user_id = author.id " +
            "where m.id = :movieId " +
            "group by m.id, author.username, user_reaction.is_like; "
    )
    Optional<MovieDetails> findMovieDetails(@Param("movieId") Long movieId, @Param("userId") Long userId);

    default Optional<MovieDetails> findMovieDetails(Long movieId){
        return findMovieDetails(movieId, -1L);
    }

    @Query(nativeQuery = true, value =
            "select * from (" +
        "select " +
            "cast(m.uuid as varchar) as uuid, " +
            "m.title as title, " +
            "m.description as description, " +
            "m.user_id as userId, " +
            "m.created_at as createdAt, " +
            "author.username as author, " +
            "count(case when reactions.is_like then 1 end) as likes, " +
            "count(case when not reactions.is_like then 1 end) as hates, " +
            "user_reaction.is_like as userReaction " +
        "from movies m " +
        "left join user_movie_reactions reactions ON m.id = reactions.movie_id " +
        "left join user_movie_reactions user_reaction ON m.id = user_reaction.movie_id and user_reaction.user_id = :userId " +
        "left join users author on m.user_id = author.id " +
        "where :authorId = -1 or author.id = :authorId " +
        "group by m.id, author.username, user_reaction.is_like " +
        ") as details order by details.createdAt desc; "
    )
    List<MovieDetails> findAllMovieDetails(@Param("userId") Long userId, @Param("authorId") Long authorId);

    default List<MovieDetails> findAllMovieDetails(){
        return findAllMovieDetails(-1L, -1L);
    }
}
