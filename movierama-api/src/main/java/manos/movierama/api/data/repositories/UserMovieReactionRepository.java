package manos.movierama.api.data.repositories;

import java.util.Optional;
import manos.movierama.api.data.entities.UserMovieReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMovieReactionRepository extends JpaRepository<UserMovieReaction, Long> {

    Optional<UserMovieReaction> getUserMovieReactionByUserIdAndMovieId(Long userId, Long moveId);
}
