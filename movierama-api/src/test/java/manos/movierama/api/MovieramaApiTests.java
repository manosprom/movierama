package manos.movierama.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(Config.class)
class MovieramaApiTests {

	@Test
	void contextLoads() {
	}

}
