package manos.movierama.api.data.repositories;

import static manos.movierama.api.data.fixtures.UserFixture.user;
import static org.assertj.core.api.Assertions.assertThat;

import manos.movierama.api.Config;
import manos.movierama.api.data.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Import(Config.class)
class UserRepositoryTests {

    @Autowired
    private UserRepository subject;

    @Test
    void shouldCreateAUser(){
        // Given
        String username = "aUsername";
        String passwordEncoded = "passwordEncoded";

        User givenNewUser = User.signUp(username, passwordEncoded);

        // When
        User savedUser = subject.save(givenNewUser);

        // Then
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo(username);
        assertThat(savedUser.getPasswordEncoded()).isEqualTo(passwordEncoded);
        assertThat(savedUser.getCreatedAt()).isNotNull();
        assertThat(savedUser.getUuid()).isNotNull();
        assertThat(savedUser.getMovies()).isEmpty();
        assertThat(savedUser.getReactions()).isEmpty();
    }

    @Test
    void shouldFindAUser_byUsername(){
        // Given
        User givenUser = subject.save(user().build());

        // When
        User savedUser = subject.findByUsernameIgnoringCase(givenUser.getUsername()).get();

        // Then
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo(givenUser.getUsername());
        assertThat(savedUser.getPasswordEncoded()).isEqualTo(givenUser.getPasswordEncoded());
        assertThat(savedUser.getCreatedAt()).isNotNull();
        assertThat(savedUser.getUuid()).isNotNull();
        assertThat(savedUser.getMovies()).isEmpty();
        assertThat(savedUser.getReactions()).isEmpty();
    }

    @Test
    void shouldFindAUser_byId(){
        // Given
        User givenUser = subject.save(user().build());

        // When
        User savedUser = subject.findById(givenUser.getId()).get();

        // Then
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo(givenUser.getUsername());
        assertThat(savedUser.getPasswordEncoded()).isEqualTo(givenUser.getPasswordEncoded());
        assertThat(savedUser.getCreatedAt()).isEqualTo(givenUser.getCreatedAt());
        assertThat(savedUser.getUuid()).isEqualTo(givenUser.getUuid());
        assertThat(savedUser.getMovies()).isEmpty();
        assertThat(savedUser.getReactions()).isEmpty();
    }

    @Test
    void shouldFindAUser_byUUID(){
        // Given
        User givenUser = subject.save(user().build());

        // When
        User savedUser = subject.findByUuid(givenUser.getUuid()).get();

        // Then
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo(givenUser.getUsername());
        assertThat(savedUser.getPasswordEncoded()).isEqualTo(givenUser.getPasswordEncoded());
        assertThat(savedUser.getCreatedAt()).isEqualTo(givenUser.getCreatedAt());
        assertThat(savedUser.getUuid()).isEqualTo(givenUser.getUuid());
        assertThat(savedUser.getMovies()).isEmpty();
        assertThat(savedUser.getReactions()).isEmpty();
    }
}
