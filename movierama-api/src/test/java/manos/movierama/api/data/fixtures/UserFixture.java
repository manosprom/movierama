package manos.movierama.api.data.fixtures;

import static java.time.LocalDateTime.now;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.data.entities.User.UserBuilder;

public class UserFixture {
    private final static AtomicLong idGenerator = new AtomicLong();

    public static UserBuilder user(){
        long id = idGenerator.incrementAndGet();
        return User.builder()
                .id(id)
                .username("username_" + id)
                .passwordEncoded("password_encoded_"+id)
                .uuid(UUID.randomUUID())
                .createdAt(now());
    }
}
