package manos.movierama.api.data.repositories;

import static manos.movierama.api.data.entities.UserMovieReaction.createHateReaction;
import static manos.movierama.api.data.entities.UserMovieReaction.createLikeReaction;
import static manos.movierama.api.data.fixtures.MovieFixture.movie;
import static manos.movierama.api.data.fixtures.UserFixture.user;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import manos.movierama.api.Config;
import manos.movierama.api.data.entities.Movie;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.data.queries.MovieDetails;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Import(Config.class)
class MovieRepositoryTests {

    @Autowired
    private MovieRepository subject;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMovieReactionRepository userMovieReactionRepository;

    @Test
    void shouldCreateMovie(){
        // Given
        User givenUser = userRepository.save(user().build());
        Movie givenMovie = movie(givenUser).build();

        // When
        Movie savedMovie = subject.save(givenMovie);

        // Then
        assertThat(savedMovie.getId()).isNotNull();
        assertThat(savedMovie.getCreatedAt()).isNotNull();
        assertThat(savedMovie.getUuid()).isNotNull();

        assertThat(savedMovie.getUser().getId()).isEqualTo(givenUser.getId());
        assertThat(savedMovie.getTitle()).isEqualTo(givenMovie.getTitle());
        assertThat(savedMovie.getDescription()).isEqualTo(givenMovie.getDescription());
    }

    @Test
    void shouldFindMovieByTitleIgnoringCase(){
        // Given
        User givenUserOne = userRepository.save(user().build());
        Movie givenMovieOne = subject.save(movie(givenUserOne).title("ATitleWithCaps").build());

        // When
        Movie foundMovie = subject.findByTitleIgnoringCase("atitlewithcaps").get();

        // Then
        assertThat(foundMovie.getId()).isNotNull();
        assertThat(foundMovie.getCreatedAt()).isNotNull();
        assertThat(foundMovie.getUuid()).isNotNull();

        assertThat(foundMovie.getUser().getId()).isEqualTo(givenUserOne.getId());
        assertThat(foundMovie.getTitle()).isEqualTo(givenMovieOne.getTitle());
        assertThat(foundMovie.getDescription()).isEqualTo(givenMovieOne.getDescription());
    }

    @Test
    void shouldGetMovieDetails(){
        // Given
        User givenUserOne = userRepository.save(user().build());
        Movie givenMovieOne = subject.save(movie(givenUserOne).build());

        User givenUserTwo = userRepository.save(user().build());
        Movie givenMovieTwo = subject.save(movie(givenUserTwo).build());

        userMovieReactionRepository.save(createLikeReaction(givenMovieOne, givenUserOne));
        userMovieReactionRepository.save(createLikeReaction(givenMovieOne, givenUserTwo));

        userMovieReactionRepository.save(createHateReaction(givenMovieTwo, givenUserOne));
        userMovieReactionRepository.save(createHateReaction(givenMovieTwo, givenUserTwo));

        // When
        List<MovieDetails> allMovieDetails = subject.findAllMovieDetails();

        // Then
        assertThat(allMovieDetails).hasSize(2);

        MovieDetails movieDetailsOne = allMovieDetails.get(1);

        assertThat(movieDetailsOne.getTitle()).isEqualTo(givenMovieOne.getTitle());
        assertThat(movieDetailsOne.getDescription()).isEqualTo(givenMovieOne.getDescription());
        assertThat(movieDetailsOne.getLikes()).isEqualTo(2);
        assertThat(movieDetailsOne.getHates()).isEqualTo(0);
        assertThat(movieDetailsOne.getUuid()).isEqualTo(givenMovieOne.getUuid());
        assertThat(movieDetailsOne.getCreatedAt()).isEqualTo(givenMovieOne.getCreatedAt());
        assertThat(movieDetailsOne.getUserReaction()).isNull();
        assertThat(movieDetailsOne.getAuthor()).isEqualTo(givenUserOne.getUsername());

        MovieDetails movieDetailsTwo = allMovieDetails.get(0);

        assertThat(movieDetailsTwo.getTitle()).isEqualTo(givenMovieTwo.getTitle());
        assertThat(movieDetailsTwo.getDescription()).isEqualTo(givenMovieTwo.getDescription());
        assertThat(movieDetailsTwo.getLikes()).isEqualTo(0);
        assertThat(movieDetailsTwo.getHates()).isEqualTo(2);
        assertThat(movieDetailsTwo.getUuid()).isEqualTo(givenMovieTwo.getUuid());
        assertThat(movieDetailsTwo.getCreatedAt()).isEqualTo(givenMovieTwo.getCreatedAt());
        assertThat(movieDetailsTwo.getUserReaction()).isNull();
        assertThat(movieDetailsTwo.getAuthor()).isEqualTo(givenUserTwo.getUsername());
    }
}
