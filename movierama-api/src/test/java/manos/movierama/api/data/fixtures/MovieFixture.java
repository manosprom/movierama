package manos.movierama.api.data.fixtures;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import manos.movierama.api.data.entities.Movie;
import manos.movierama.api.data.entities.Movie.MovieBuilder;
import manos.movierama.api.data.entities.User;
import manos.movierama.api.data.entities.User.UserBuilder;

public class MovieFixture {
    private final static AtomicLong idGenerator = new AtomicLong();

    public static MovieBuilder movie(){
        User user = UserFixture.user().build();
        return movie(user);
    }

    public static MovieBuilder movie(User user){
        long id = idGenerator.incrementAndGet();


        return Movie.builder()
                .id(id)
                .title("title_" + id)
                .description("description_" + id)
                .user(user)
                .uuid(randomUUID())
                .createdAt(now())
                .createdAt(now());
    }
}
