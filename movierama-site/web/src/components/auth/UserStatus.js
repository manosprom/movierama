import {Button, makeStyles} from "@material-ui/core";
import {Link as RouterLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../redux/actions/auth/AuthActions";

const useStyles = makeStyles({
    root: {
        color: "black"
    },
    unauthenticatedRoot: {

    },
    authenticatedRoot: {

    }
});

const UnauthenticatedStatusBar = () => {
    const classes = useStyles();
    return (
        <div className={classes.unauthenticatedRoot}>
            <Button component={RouterLink} to="/user/login">Log in</Button> <span>or</span>
            <Button component={RouterLink} to="/user/signup">Sign Up</Button>
        </div>
    )
}

const AuthenticatedStatusBar = ({username}) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    return (
        <div className={classes.authenticatedRoot}>
            <p>Welcome Back: {username}
            <Button onClick={() => {dispatch(logout())}}>Logout</Button>
            </p>
        </div>
    )
}

export const UserStatus = () => {
    const classes = useStyles();

    const { isLoggedIn, user } = useSelector(state => state.auth)
    return (

        <div className={classes.root}>
            {
                !isLoggedIn ? <UnauthenticatedStatusBar/> : <AuthenticatedStatusBar username={user.username}/>
            }
        </div>
    )
}