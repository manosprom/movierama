import React, {useEffect} from 'react'
import {MovieCard} from "./MovieCard";
import {Link as RouterLink, useParams} from "react-router-dom";
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Button from "@material-ui/core/Button";
import {getMovies} from "../../redux/actions/movies/MovieActions";

export const MovieCardList = () => {
    const dispatch = useDispatch()
    const movies = useSelector(state => state.movies.items)
    const {isLoggedIn} = useSelector(state => state.auth)

    let params = useParams();

    useEffect(() => {
        (async () => {
            await dispatch(getMovies(params["username"]))
        })()
    }, [isLoggedIn, dispatch, params])

    let leftSize = 12;
    let rightSize = 0

    if (isLoggedIn) {
        leftSize = 9
        rightSize = 3
    }

    return (
        <Grid container>
            <Grid item xs={leftSize} className="movies-list">
                {movies.map((movie, index) => <MovieCard key={index} movie={movie}/>)}
            </Grid>

            {
                rightSize > 0 ?
                    <Grid item xs={rightSize} className="movies-sidebar">
                        <Button component={RouterLink} to="/movies/create">Add Movie</Button>
                    </Grid> :
                    null
            }
        </Grid>
    )
}