import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";
import {createMovie} from "../../redux/actions/movies/MovieActions";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export const MovieForm = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const history = useHistory();

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")

    const errors = useSelector(state => state.movies.errors)

    const handleMovieCreation = async (e) => {
        e.preventDefault()
        try {
            const response = await dispatch(createMovie(title, description));
            if (response.title) {
                history.push("/movies/")
            }
        } catch (error) {

        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Create Movie
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="title"
                        label="Title"
                        name="title"
                        onChange={(e) => setTitle(e.target.value)}
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="description"
                        label="Description"
                        type="description"
                        id="description"
                        onChange={(e) => setDescription(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        onClick={handleMovieCreation}
                        className={classes.submit}
                    > Create Movie</Button>
                </form>
                {errors ? errors.map(message => (<p>{message}</p>)) : null}
            </div>
        </Container>
    );
}