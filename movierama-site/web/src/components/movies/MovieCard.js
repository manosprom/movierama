import React from 'react'
import {Box, Button, Card, CardActions, CardContent, CardHeader, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import {Link as RouterLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {hateMovie, likeMovie} from "../../redux/actions/movies/MovieActions";

const useStyles = makeStyles({
    root: {
        borderColor: "1px solid black"
    },
    header: {
        textAlign: "left"
    },
    content: {
        textAlign: "justify"
    },
    buttonHateActive: {
        backgroundColor: "red"
    },
    buttonLikeActive: {
        backgroundColor: "blue"
    }
});

const SubHeader = (props) => {
    const {user, postedBefore} = props

    return (
        <Grid container>
            <Box>
                Posted by <Link to={`/user/${user}/movies/`} component={RouterLink}>{user}</Link>
            </Box>
            <span>,</span>
            <Box marginLeft={1}>
                {postedBefore}
            </Box>
        </Grid>
    )
}

const LikeFooter = ({title, likes, hates, userReaction}) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const {isLoggedIn} = useSelector(state => state.auth)

    return (
        <Grid container>
            <Box justifyContent="flex-start">
                <Button size="small"
                        className={userReaction === true ? classes.buttonLikeActive : null}
                        disabled={!isLoggedIn}
                        onClick={() => dispatch(likeMovie(title))}>{likes} likes</Button>
                <Button size="small"
                        className={userReaction === false ? classes.buttonHateActive : null}
                        disabled={!isLoggedIn}
                        onClick={() => dispatch(hateMovie(title))}
                > {hates} hates
                </Button>
            </Box>
            <Box flexGrow={1}/>
            <Box justifyContent="flex-end">
                {userReaction === true ? "You like this movie" : null}
                {userReaction === false ? "You hate this movie" : null}
            </Box>
        </Grid>
    )
}

export const MovieCard = ({movie}) => {
    const classes = useStyles();
    return (
        <Box paddingTop={1} paddingBottom={1}>
            <Card className={classes.root} variant="outlined">
                <CardHeader
                    className={classes.header}
                    title={movie.title}
                    subheader={
                        <SubHeader user={movie.author} postedBefore={movie.postedBefore}/>
                    }
                />
                <CardContent>
                    <Typography className={classes.content} gutterBottom component="p">
                        {movie.description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <LikeFooter
                        title={movie.title}
                        likes={movie.likes}
                        hates={movie.hates}
                        userReaction={movie.userReaction}
                    />
                </CardActions>
            </Card>
        </Box>
    )
}