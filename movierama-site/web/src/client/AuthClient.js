import {createPathUrl, dataFetch} from "./Client";

export const login = async (username, password) => {
    const url = createPathUrl("auth/login")
    const requestData = {
        "username": username,
        "password": password
    }
    const response = await dataFetch({method: "POST", url: url, requestData: requestData});
    const data = await response.json();
    if (response.status === 200) {
        if (data.token) {
            localStorage.setItem("user", JSON.stringify(data));
        }
        return data;
    } else {
        throw data;
    }
}

export const register = async (username, password) => {
    const url = createPathUrl("auth/register")
    const requestData = {
        "username": username,
        "password": password
    }
    const response = await dataFetch({method: "POST", url: url, requestData: requestData});
    const data = await response.json();

    if (response.status === 200) {
        if (data.token) {
            localStorage.setItem("user", JSON.stringify(data));
        }
        return data;
    } else {
        throw data;
    }
}

export const logout = () => {
    localStorage.removeItem("user");
}

const AuthClient = {
    login,
    logout,
    register
}

export default AuthClient;