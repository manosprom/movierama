import {createPathUrl, dataFetch} from "./Client";

export const getUserMovies = async (username) => {
    const url = createPathUrl(`movies?author=${username}`,)
    const response = await dataFetch({method: "GET", url: url});
    const data = await response.json();
    if ([200].includes(response.status)) {
        return data
    } else {
        throw data;
    }
}

export const getMovies = async () => {
    const url = createPathUrl("movies")
    const response = await dataFetch({method: "GET", url: url});
    return await response.json();
}

export const createMovie = async (movieTitle, movieDescription) => {
    const url = createPathUrl("movies")
    const requestData = {
        "title": movieTitle,
        "description": movieDescription
    }
    const response = await dataFetch({method: "POST", url: url, requestData: requestData});
    const data = await response.json();
    if ([200, 201].includes(response.status)) {
        return data
    } else {
        throw data;
    }
}

export const likeMovie = async (movieName) => {
    try {
        const url = createPathUrl(`movies/${movieName}/like`)
        const response = await dataFetch({method: "PUT", url: url,});
        return await response.json();
    } catch (err) {
        console.log(err);
    }
}

export const hateMovie = async (movieName) => {
    try {
        const url = createPathUrl(`movies/${movieName}/hate`)
        const response = await dataFetch({method: "PUT", url: url,});
        return await response.json();
    } catch (err) {
        console.log(err);
    }
};

const MoviesClient = {
    createMovie,
    getUserMovies,
    getMovies,
    likeMovie,
    hateMovie
};

export default MoviesClient;