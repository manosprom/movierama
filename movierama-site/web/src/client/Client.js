const getUser = () => {
    return JSON.parse(localStorage.getItem("user"))
}

export const createPathUrl = (subPath, tokens) => {
    let path = process.env.REACT_APP_BACKEND_BASE_PATH + subPath;

    if (tokens) {
        for (const [key, value] of Object.entries(tokens)) {
            const replacer = new RegExp(key, 'g')
            path = path.replace(replacer, value)
        }
    }
    return path;
}

export const dataFetch = async ({method, url, requestData}) => {
    const headers = {
        'Content-type': 'application/json; charset=UTF-8'
    }
    const user = getUser()
    if (user) {
        headers['Authorization'] = `Bearer ${user.token}`
    }

    const init = {};
    init.method = method;
    init.headers = headers

    if (requestData) {
        init.body = JSON.stringify(requestData)
    }

    return await fetch(url, init);
}