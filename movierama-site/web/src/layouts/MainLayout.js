import {AppBar, Box, Container, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {UserStatus} from "../components/auth/UserStatus";
import {Link as RouterLink} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    appbar: {
        background: "transparent"
    },
    title: {
        flexGrow: 1,
        color: "black",
        textDecoration: "none"
    },
}));

export const MainLayout = ({children}) => {
    const classes = useStyles();

    return (
        <Container>
            <AppBar position="static" className={classes.appbar}>
                <Toolbar>
                    <RouterLink to="/" className={classes.title}>
                        <Typography variant="h4" className={classes.title} align="left">
                            MovieRama
                        </Typography>
                    </RouterLink>
                    <Box flexGrow={1}/>
                    <UserStatus/>
                </Toolbar>
            </AppBar>
            <Box margin={5}/>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        </Container>
    )
}