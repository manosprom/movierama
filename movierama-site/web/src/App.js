import './App.css';
import {MainLayout} from "./layouts/MainLayout";
import {RegistrationForm} from "./components/auth/RegistrationForm";
import {MovieCardList} from "./components/movies/MovieCardList";
import {LoginForm} from "./components/auth/LoginForm";

import {Route, Switch} from 'react-router-dom';
import {useSelector} from "react-redux";
import {MovieForm} from "./components/movies/MovieForm";

function RouteWrapper({
    component: Component,
    layout: Layout,
    ...rest
}) {
    return (
        <Route {...rest} render={(props) =>
            <Layout {...props}>
                <Component {...props} />
            </Layout>
        }/>
    );
}

const App = () => {
    const {isLoggedIn} = useSelector(state => state.auth)
    return (
        <div className="App">
            <Switch>
                {!isLoggedIn ? <RouteWrapper exact path="/user/login" component={LoginForm} layout={MainLayout}/> : null}
                {!isLoggedIn ? <RouteWrapper exact path="/user/signup" component={RegistrationForm} layout={MainLayout}/> : null}
                <RouteWrapper exact path="/" component={MovieCardList} layout={MainLayout}/>
                <RouteWrapper exact path="/movies/" component={MovieCardList} layout={MainLayout}/>
                <RouteWrapper exact path="/movies/create" component={MovieForm} layout={MainLayout} />
                <RouteWrapper path="/user/:username/movies/" component={MovieCardList} layout={MainLayout}/>
            </Switch>
        </div>
    );
}

export default App;