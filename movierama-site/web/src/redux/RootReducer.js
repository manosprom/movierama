import {combineReducers} from "redux";
import AuthReducer from './reducers/AuthReducer';
import MoviesReducer from "./reducers/MoviesReducer";

const rootReducer = combineReducers({
    auth: AuthReducer,
    movies: MoviesReducer
})

export default rootReducer;