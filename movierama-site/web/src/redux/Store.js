import { applyMiddleware, createStore } from 'redux';

import rootReducer from './RootReducer';
import thunk from "redux-thunk";
import logger from 'redux-logger';

const middlewares = [thunk];

if(process.env.NODE_ENV === 'development'){
    middlewares.push(logger);
}

export const store = createStore(rootReducer, applyMiddleware(...middlewares));