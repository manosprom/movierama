import AuthTypes from "./AuthTypes";

import AuthClient from "../../../client/AuthClient";

export const register = (username, password) => async (dispatch, getState) => {
    try {
        const registrationResponse = await AuthClient.register(username, password);
        dispatch({
            type: AuthTypes.REGISTER_SUCCESS,
            payload: {user: registrationResponse}
        })
        return registrationResponse
    } catch (error) {
        console.log(error)
        dispatch({
            type: AuthTypes.REGISTER_FAIL,
            payload: {
                errors: error.details
            }
        });
        throw error;
    }
};

export const login = (username, password) => async (dispatch, getState) => {
    try {
        const loginResponse = await AuthClient.login(username, password)
        dispatch({
            type: AuthTypes.LOGIN_SUCCESS,
            payload: {user: loginResponse},
        });
        return loginResponse;
    } catch (error) {
        console.log(error)
        dispatch({
            type: AuthTypes.LOGIN_FAIL,
            payload: {
                errors: error.details
            }
        });

        throw error
    }
};

export const logout = () => (dispatch) => {
    AuthClient.logout();

    dispatch({
        type: AuthTypes.LOGOUT,
    });
};