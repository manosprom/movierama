const MovieTypes = {
    MOVIES_FETCH_SUCCESS: "MOVIES_FETCH_SUCCESS",
    MOVIES_FETCH_FAILURE: "MOVIES_FETCH_FAILURE",
    MOVIE_CREATE_SUCCESS: "MOVIE_CREATE_SUCCESS",
    MOVIE_CREATE_FAILURE: "MOVIE_CREATE_FAILURE",
    MOVIE_LIKE_SUCCESS: "MOVIE_LIKE_SUCCESS",
    MOVIE_LIKE_FAILURE: "MOVIE_LIKE_FAILURE",
    MOVIE_HATE_SUCCESS: "MOVIE_HATE_SUCCESS",
    MOVIE_HATE_FAILURE: "MOVIE_HATE_FAILURE"
}

export default MovieTypes;
