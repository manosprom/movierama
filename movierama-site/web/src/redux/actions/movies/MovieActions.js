import MovieTypes from "./MovieTypes";

import MoviesClient from "../../../client/MoviesClient";

export const createMovie = (title, description) => async (dispatch, getState) => {
    try {
        const createdMovie = await MoviesClient.createMovie(title, description);

        dispatch({
            type: MovieTypes.MOVIE_CREATE_SUCCESS,
            payload: {
                createdItem: createdMovie,
            }
        })

        return createdMovie;
    } catch (error) {
        dispatch({
            type: MovieTypes.MOVIE_CREATE_FAILURE,
            payload: {
                errors: error.details
            }
        });
        throw error;
    }
}

export const getMovies = (author) => async (dispatch, getState) => {
    try {
        let movies;
        if (author) {
            console.log(author);
            movies = await MoviesClient.getUserMovies(author);
        } else {
            movies = await MoviesClient.getMovies();
        }
        dispatch({
            type: MovieTypes.MOVIES_FETCH_SUCCESS,
            payload: {
                movies: movies,
                username: null
            }
        })
        return movies
    } catch (error) {
        dispatch({
            type: MovieTypes.MOVIES_FETCH_FAILURE,
        });
        throw error;
    }
};

export const likeMovie = (movieTitle) => async (dispatch, getState) => {
    try {
        const movieResponse = await MoviesClient.likeMovie(movieTitle)
        dispatch({
            type: MovieTypes.MOVIE_LIKE_SUCCESS,
            payload: {
                updatedItem: movieResponse,
            },
        });
    } catch (error) {
        console.log(error)
        dispatch({
            type: MovieTypes.MOVIE_LIKE_FAILURE,
        });
        throw error;
    }
};

export const hateMovie = (movieTitle) => async (dispatch, getState) => {
    try {
        const movieResponse = await MoviesClient.hateMovie(movieTitle)
        dispatch({
            type: MovieTypes.MOVIE_HATE_SUCCESS,
            payload: {
                updatedItem: movieResponse,
            },
        });
    } catch (error) {
        dispatch({
            type: MovieTypes.MOVIE_HATE_SUCCESS,
        });
        throw error;
    }
};