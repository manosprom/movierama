import MovieTypes from "../actions/movies/MovieTypes";
import {addMovieItem, updateMovieItems} from "./MovieUtils";

const initialState = {
    items: [],
    errors: null,
    username: null
}

const MovieReducer = (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case MovieTypes.MOVIES_FETCH_SUCCESS:
            return {
                ...state,
                items: payload.movies,
                username: payload.username,
            };
        case MovieTypes.MOVIES_FETCH_FAILURE:
            return {
                ...state,
                items: [],
                username: null
            };
        case MovieTypes.MOVIE_CREATE_SUCCESS:
            return {
                ...state,
                items: addMovieItem(state.items, payload.createdItem),
                username: payload.username
            };
        case MovieTypes.MOVIE_CREATE_FAILURE:
            return {
                ...state,
                errors: payload.errors
            };
        case MovieTypes.MOVIE_LIKE_SUCCESS:
            return {
                ...state,
                items: updateMovieItems(state.items, payload.updatedItem),
                username: payload.username
            };
        case MovieTypes.MOVIE_LIKE_FAILURE:
            return {
                ...state,
            };
        case MovieTypes.MOVIE_HATE_SUCCESS:
            return {
                ...state,
                items: updateMovieItems(state.items, payload.updatedItem),
                username: payload.username
            };
        case MovieTypes.MOVIE_HATE_FAILURE:
            return {
                ...state,
            };
        default:
            return state;
    }
}

export default MovieReducer;