import AuthTypes from "../actions/auth/AuthTypes";

const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
    ? { isLoggedIn: true, user }
    : { isLoggedIn: false, user: null, errors: null };

const AuthReducer =  (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case AuthTypes.REGISTER_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: payload.user,
            };
        case AuthTypes.REGISTER_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                errors: payload.errors
            };
        case AuthTypes.LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: payload.user,
            };
        case AuthTypes.LOGIN_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
                errors: payload.errors
            };
        case AuthTypes.LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        default:
            return state;
    }
}

export default AuthReducer;