export const updateMovieItems = (currentItems, updatedItem) => {
    return currentItems.map((item) => item.uuid === updatedItem.uuid ? updatedItem: item)
}

export const addMovieItem = (currentItems, newItem) => {
    currentItems.unshift(newItem);
    return currentItems;
}